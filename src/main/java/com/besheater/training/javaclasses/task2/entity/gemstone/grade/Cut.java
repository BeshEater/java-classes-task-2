package com.besheater.training.javaclasses.task2.entity.gemstone.grade;

/**
 * According to G.I.A. gemstone grading system.
 */
public enum  Cut {
    VERY_GOOD ("Very good"),
    GOOD ("Good"),
    MEDIUM ("Medium"),
    POOR ("Poor");

    private final String name;

    Cut(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}