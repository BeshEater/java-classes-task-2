package com.besheater.training.javaclasses.task2;

import com.besheater.training.javaclasses.task2.entity.Jewelry;
import com.besheater.training.javaclasses.task2.entity.gemstone.Gemstone;
import com.besheater.training.javaclasses.task2.repository.GemstoneRepository;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class App {
    private static final GemstoneRepository gemstoneRepository = new GemstoneRepository();

    public static void main(String[] args) {
        // Set Locale to english to format output accordingly
        Locale.setDefault(Locale.ENGLISH);

        // Create new jewelry with some gemstones
        List<Gemstone> gemstones = gemstoneRepository.getAll();
        Jewelry jewelry = new Jewelry(gemstones);
        System.out.println("New jewelry gems:");
        System.out.println(Gemstone.prettyPrintGemstones(jewelry.getGemstones()));

        // Get jewelry weight and price
        System.out.println(String.format("Jewelry weight = %.2f carat", jewelry.getWeight()));
        System.out.println(String.format("Jewelry price = %.2f $", jewelry.getPrice() / 100.0));

        // Sort gemstones in jewelry by price
        List<Gemstone> gemstonesSortedByPrice = jewelry.getGemstones()
                                                        .stream()
                                                        .sorted(Gemstone::compareByPrice)
                                                        .collect(Collectors.toList());
        System.out.print("\n");
        System.out.println("Gemstones in jewelry sorted by price:");
        System.out.println(Gemstone.prettyPrintGemstones(gemstonesSortedByPrice));

        // Find gemstones that somewhat transparent (transparency from 0.3 to 0.5)
        List<Gemstone> transparentGemstones = jewelry.getGemstones()
                                                     .stream()
                                                     .filter(g -> g.isTransparencyInRange(0.3, 0.5))
                                                     .collect(Collectors.toList());
        System.out.print("\n");
        System.out.println("Gemstones in jewelry with transparency from 0.3 to 0.5:");
        System.out.println(Gemstone.prettyPrintGemstones(transparentGemstones));
    }
}