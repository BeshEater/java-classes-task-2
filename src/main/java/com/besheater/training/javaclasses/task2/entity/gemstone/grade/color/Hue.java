package com.besheater.training.javaclasses.task2.entity.gemstone.grade.color;

/**
 * According to G.I.A. gemstone grading system.
 */
public enum Hue {
    PURPLE_RED ("Purple red"),
    STRONGLY_PURPLISH_RED ("Strongly purplish red"),
    SLIGHTLY_PURPLISH_RED ("Slightly purplish red"),
    RED ("Red"),
    ORANGY_RED ("Orangy red"),
    ORANGE_RED ("Orange red"),

    REDDISH_ORANGE ("Reddish orange"),
    ORANGE ("Orange"),
    YELLOWISH_ORANGE ("Yellowish orange"),
    ORANGY_YELLOW ("Orangy yellow"),
    YELLOW ("Yellow"),
    GREENISH_YELLOW ("Greenish yellow"),

    YELLOW_GREEN ("Yellow green"),
    STRONGLY_YELLOWISH_GREEN ("Strongly yellowish green"),
    YELLOWISH_GREEN ("Yellowish green"),
    SLIGHTLY_YELLOWISH_GREEN ("Slightly yellowish green"),
    GREEN ("Green"),
    VERY_SLIGHTLY_BLUISH_GREEN ("Very slightly bluish green"),

    BLUISH_GREEN ("Bluish green"),
    VERY_STRONGLY_BLUISH_GREEN ("Very strongly bluish green"),
    GREEN_BLUE ("Green blue"),
    VERY_STRONGLY_GREENISH_BLUE ("Very strongly greenish blue"),
    GREENISH_BLUE ("Greenish blue"),
    VERY_SLIGHTLY_GREENISH_BLUE ("Very slightly greenish blue"),

    BLUE ("Blue"),
    BLUISH_VIOLET ("Bluish violet"),
    VIOLETISH_BLUE ("Violetish blue"),
    VIOLET ("Violet"),
    BLUISH_PURPLE ("Bluish purple"),
    PURPLE ("Purple"),

    REDDISH_PURPLE ("Reddish purple");

    private final String name;

    Hue(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}