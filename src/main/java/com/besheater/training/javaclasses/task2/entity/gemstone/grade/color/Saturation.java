package com.besheater.training.javaclasses.task2.entity.gemstone.grade.color;

/**
 * According to G.I.A. gemstone grading system.
 */
public enum Saturation {
    GRAYISH ("Grayish"),
    SLIGHTLY_GRAYISH ("Slightly grayish"),
    VERY_SLIGHTLY_GRAYISH ("Very slightly grayish"),
    MODERATELY_STRONG ("Moderately strong"),
    STRONG ("Strong"),
    VIVID ("Vivid");

    private final String name;

    Saturation(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}