package com.besheater.training.javaclasses.task2.entity;

import com.besheater.training.javaclasses.task2.entity.gemstone.Gemstone;

import java.util.List;
import java.util.Objects;

public class Jewelry {
    private final List<Gemstone> gemstones;

    public Jewelry(List<Gemstone> gemstones) {
        this.gemstones = gemstones;
    }

    public List<Gemstone> getGemstones() {
        return gemstones;
    }

    public double getWeight() {
        return gemstones.stream().mapToDouble(Gemstone::getCarat).sum();
    }

    public long getPrice() {
        return gemstones.stream().mapToLong(Gemstone::getPrice).sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Jewelry jewelry = (Jewelry) o;
        return gemstones.equals(jewelry.gemstones);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gemstones);
    }

    @Override
    public String toString() {
        return "Jewelry{" +
                "gemstones=" + gemstones +
                '}';
    }
}