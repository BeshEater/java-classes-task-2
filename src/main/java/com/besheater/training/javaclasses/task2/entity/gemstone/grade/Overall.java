package com.besheater.training.javaclasses.task2.entity.gemstone.grade;

/**
 * According to G.I.A. gemstone grading system.
 */
public enum  Overall {
    AAA ("AAA"),
    AA ("AA"),
    A ("A"),
    B ("B"),
    C ("C");

    private final String name;

    Overall(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}