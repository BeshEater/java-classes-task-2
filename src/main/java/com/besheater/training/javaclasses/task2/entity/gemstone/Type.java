package com.besheater.training.javaclasses.task2.entity.gemstone;

public enum Type {
    DIAMOND ("Diamond"),
    RUBY ("Ruby"),
    SAPPHIRE ("Sapphire"),
    EMERALD ("Emerald"),
    AQUAMARINE ("Aquamarine"),
    AMETHYST ("Amethyst"),
    GOSHENITE ("Goshenite"),
    MALACHITE ("Malachite"),
    PYRITE ("Pyrite"),
    JASPER ("Jasper"),
    TURQUOISE ("Turquoise"),
    QUARTZ ("Quartz"),
    TOURMALINE ("Tourmaline"),
    SUGILITE ("Sugilite"),
    AGATE ("Agate"),
    LAPIS_LAZULI ("Lapis lazuli"),
    CHRYSOCOLA ("Chrysocolla"),
    OPAL ("Opal"),
    PERIDOT ("Peridot");

    private final String name;

    Type(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}