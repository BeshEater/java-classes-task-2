package com.besheater.training.javaclasses.task2.entity.gemstone.grade.color;

/**
 * According to G.I.A. gemstone grading system.
 */
public enum  Tone {
    _1 ("1 - Very very light"),
    _2 ("2 - Very light"),
    _3 ("3 - Light"),
    _4 ("4 - Light gray"),
    _5 ("5 - Gray"),
    _6 ("6 - Gray"),
    _7 ("7 - Dark gray"),
    _8 ("8 - Dark"),
    _9 ("9 - Very dark"),
    _10 ("10 - Very very dark");

    private final String name;

    Tone(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}