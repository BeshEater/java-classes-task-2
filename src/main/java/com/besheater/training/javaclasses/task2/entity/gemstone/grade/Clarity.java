package com.besheater.training.javaclasses.task2.entity.gemstone.grade;

/**
 * According to G.I.A. gemstone grading system.
 */
public enum  Clarity {
    IF ("Internally flawless"),
    VVS ("Very very small inclusions"),
    VS ("Very small inclusions"),
    SI ("Small inclusions"),
    L1 ("1st pique"),
    L2 ("2nd pique"),
    L3 ("3rd pique");

    private final String name;

    Clarity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}