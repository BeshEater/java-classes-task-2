package com.besheater.training.javaclasses.task2.repository;

import com.besheater.training.javaclasses.task2.entity.gemstone.Gemstone;
import com.besheater.training.javaclasses.task2.entity.gemstone.Type;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.Clarity;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.Cut;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.Overall;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.color.Color;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.color.Hue;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.color.Saturation;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.color.Tone;

import java.util.ArrayList;
import java.util.List;

public class GemstoneRepository {
    private final List<Gemstone> gemstones = new ArrayList<>();

    public GemstoneRepository() {
        add(new Gemstone(Type.AMETHYST, Cut.GOOD,
                         new Color(Hue.VIOLETISH_BLUE, Saturation.STRONG, Tone._7),
                         Clarity.VS, 1.34, 0.12, Overall.AA, 25000));
        add(new Gemstone(Type.SAPPHIRE, Cut.VERY_GOOD,
                         new Color(Hue.BLUE, Saturation.MODERATELY_STRONG, Tone._4),
                         Clarity.VVS, 0.95, 0.34, Overall.AA, 174500));
        add(new Gemstone(Type.MALACHITE, Cut.MEDIUM,
                         new Color(Hue.GREENISH_BLUE, Saturation.GRAYISH, Tone._8),
                         Clarity.SI, 2.3, 0.08, Overall.A, 18500));
        add(new Gemstone(Type.TOURMALINE, Cut.GOOD,
                         new Color(Hue.REDDISH_PURPLE, Saturation.VIVID, Tone._8),
                         Clarity.VVS, 1.8, 0.47, Overall.AAA, 84900));
        add(new Gemstone(Type.AGATE, Cut.VERY_GOOD,
                         new Color(Hue.ORANGY_YELLOW, Saturation.VERY_SLIGHTLY_GRAYISH, Tone._7),
                         Clarity.SI, 2.9, 0.07, Overall.A, 20300));
    }

    public List<Gemstone> getAll() {
        return new ArrayList<>(gemstones); // defensive copy
    }

    public void add(Gemstone gemstone) {
        gemstones.add(gemstone);
    }
}
