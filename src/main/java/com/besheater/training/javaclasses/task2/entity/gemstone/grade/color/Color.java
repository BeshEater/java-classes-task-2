package com.besheater.training.javaclasses.task2.entity.gemstone.grade.color;

import java.util.Objects;

/**
 * According to G.I.A. gemstone grading system.
 */
public class Color {
    private final Hue hue;
    private final Saturation saturation;
    private final Tone tone;

    public Color(Hue hue, Saturation saturation, Tone tone) {
        this.hue = hue;
        this.saturation = saturation;
        this.tone = tone;
    }

    public Hue getHue() {
        return hue;
    }

    public Saturation getSaturation() {
        return saturation;
    }

    public Tone getTone() {
        return tone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Color color = (Color) o;
        return hue == color.hue &&
                saturation == color.saturation &&
                tone.equals(color.tone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hue, saturation, tone);
    }

    @Override
    public String toString() {
        return "Color{" +
                "hue=" + hue +
                ", saturation=" + saturation +
                ", tone=" + tone +
                '}';
    }
}