package com.besheater.training.javaclasses.task2.entity.gemstone;

import com.besheater.training.javaclasses.task2.entity.gemstone.grade.Clarity;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.Cut;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.Overall;
import com.besheater.training.javaclasses.task2.entity.gemstone.grade.color.Color;

import java.util.List;
import java.util.Objects;

public class Gemstone {
    public static final double MIN_TRANSPARENCY = 0;
    public static final double MAX_TRANSPARENCY = 1;

    private final Type type;
    private final Cut cut;
    private final Color color;
    private final Clarity clarity;
    private final double carat; // weight
    private final double transparency;
    private final Overall overall;
    private final long price;

    public Gemstone(Type type, Cut cut, Color color, Clarity clarity,
                    double carat, double transparency, Overall overall, long price) {
        if (type == null || cut == null || color == null || clarity == null || overall == null) {
            throw new IllegalArgumentException("Values cannot be null");
        }
        checkCarat(carat);
        checkTransparency(transparency);
        checkPrice(price);
        this.type = type;
        this.color = color;
        this.clarity = clarity;
        this.cut = cut;
        this.carat = carat;
        this.transparency = transparency;
        this.overall = overall;
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public Cut getCut() {
        return cut;
    }

    public Color getColor() {
        return color;
    }

    public Clarity getClarity() {
        return clarity;
    }

    public double getCarat() {
        return carat;
    }

    public double getTransparency() {
        return transparency;
    }

    public Overall getOverall() {
        return overall;
    }

    public long getPrice() {
        return price;
    }

    private void checkCarat(double carat) {
        if (carat < 0) {
            throw new IllegalArgumentException("Weight in carat cannot be less than 0");
        }
    }

    private void checkPrice(long price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be less than 0");
        }
    }

    private void checkTransparency(double transparency) {
        if (transparency < MIN_TRANSPARENCY || transparency > MAX_TRANSPARENCY) {
            String message = String.format("Transparency is not in range. " +
                            "It should be between %.0f and %.0f inclusive",
                             MIN_TRANSPARENCY, MAX_TRANSPARENCY);
            throw new IllegalArgumentException(message);
        }
    }

    public boolean isTransparencyInRange(double from, double to) {
        checkTransparency(from);
        checkTransparency(to);
        if (to < from) {
            throw new IllegalArgumentException("'To' cannot be less than 'from'");
        }
        return transparency >= from && transparency <= to;
    }

    public static int compareByPrice(Gemstone first, Gemstone second) {
        return Long.compare(first.price, second.price);
    }

    public static int compareByWeight(Gemstone first, Gemstone second) {
        return Double.compare(first.carat, second.carat);
    }

    public static int compareByTransparency(Gemstone first, Gemstone second) {
        return Double.compare(first.transparency, second.transparency);
    }

    public static String prettyPrintGemstones(List<Gemstone> gemstones) {
        StringBuilder builder = new StringBuilder();
        String prefix =    "*********************************************************************************************************************************************************\n";
        String suffix =    "*********************************************************************************************************************************************************";
        String separator = "---------------------------------------------------------------------------------------------------------------------------------------------------------\n";
        builder.append(prefix);
        builder.append(String.format("%14s%12s%25s%25s%20s%10s" +
                        "%8s%14s%10s%10s\n",
                "Type", "Cut", "Hue", "Saturation", "Tone", "Clarity",
                "Carat", "Transparency", "Overall", "Price, $"));
        builder.append(separator);
        for (Gemstone gemstone : gemstones) {
            builder.append(String.format("%14s", gemstone.getType().getName()));
            builder.append(String.format("%12s", gemstone.getCut().getName()));
            builder.append(String.format("%25s", gemstone.getColor().getHue().getName()));
            builder.append(String.format("%25s", gemstone.getColor().getSaturation().getName()));
            builder.append(String.format("%20s", gemstone.getColor().getTone().getName()));
            builder.append(String.format("%10s", gemstone.getClarity()));
            builder.append(String.format("%8.2f", gemstone.getCarat()));
            builder.append(String.format("%14.2f", gemstone.getTransparency()));
            builder.append(String.format("%10s", gemstone.getOverall()));
            builder.append(String.format("%10.2f", gemstone.getPrice() / 100.0));
            builder.append("\n");
        }
        builder.append(suffix);
        return builder.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, cut, color, clarity, carat, transparency, overall, price);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gemstone gemstone = (Gemstone) o;
        return Double.compare(gemstone.carat, carat) == 0 &&
                Double.compare(gemstone.transparency, transparency) == 0 &&
                price == gemstone.price &&
                type.equals(gemstone.type) &&
                cut == gemstone.cut &&
                color.equals(gemstone.color) &&
                clarity == gemstone.clarity &&
                overall == gemstone.overall;
    }

    @Override
    public String toString() {
        return "Gemstone{" +
                "type=" + type +
                ", cut=" + cut +
                ", color=" + color +
                ", clarity=" + clarity +
                ", carat=" + carat +
                ", transparency=" + transparency +
                ", overall=" + overall +
                ", price=" + price +
                '}';
    }
}